<?php
//php.ini tiedoston muokkaus funktiot, joilla asetetaan tiedostokoko rajoitukset ja estetään php:n paljastuminen nettiin
ini_set('post_max_size', '1M'); 
ini_set('upload_max_filesize', '1M');
ini_set('expose_php', 'Off');

$target_dir = "secure_uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$fileType = pathinfo($target_file,PATHINFO_EXTENSION);
$sizeLimit = 1000 * 1000;
// Luodaan satunnainen merkkijono tiedoston nimeksi
$fileName = sha1(time()*mt_rand());
$target_file = $target_dir.$fileName.'.'.$fileType;
// Tarkistetaan tiedoston MIME-tyyppi
// Jos haluttaisiin sallia tai estää ainoastaan tietyntyyppisiä tiedostoja, 
// niin se tehtäisiin MIME-tyypiä tarkastamalla, koska tiedostopäätteet eivät ole luotettavia.
$mime = mime_content_type($_FILES["fileToUpload"]["tmp_name"]);
if(isset($_POST["submit"])) {
    if ($_FILES["fileToUpload"]['size'] < $sizeLimit) {
        // Muokataan tiedoston oikeudet
        chmod($_FILES["fileToUpload"]["tmp_name"],0644);
        move_uploaded_file($_FILES["fileToUpload"]["tmp_name"],$target_file);
        echo "Actual mime: ".$mime."</br><a href='".$target_file."'>".$target_file."</a>";
    } else {
        echo 'File too big!';
    }
}