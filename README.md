

### Tämän projektin tarkoitus on osoittaa kuinka php:ssä voidaan turvallisesti, tai ainakin turvallisemmin, ladata tiedostoja palvelimelle.


---
**index.php** tiedostosta löytyy kaksi tiedostonlatauslomaketta, yksi on nimeltään
*Insecure Upload* ja käyttää **upload.php**-skriptiä, toinen on *Secure Upload* ja
käyttää **secureupload.php**-skriptiä.

---
Tiedosto **upload.php** sisältää perus tiedostonlataus koodia, joka ottaa POST 
metodilla lähetetyn tiedoston ja siirtää sen *uploads* nimiseen kansioon. Sen kautta
on ladattu **example.php** tiedosto, jonka voi ajaa osoittamalla selaimen tiedoston sijaintiin.
Tiedostossa on esimerkkimielessä skripti joka luo tekstitiedoston **virus.txt**.

Mahdollisen hyökkääjän olisi helppo ladata palvelimelle esimerkiksi php skripti,
jonka voisi ajaa avaamalla ladattu tiedosto. Hyökkääjä voisi myös ladata *root* 
kansioon vaikkapa **.htaccess** tiedoston ja muokkaa kansiolle asetettuja rajoituksia.
 Skripti voisi tehdä myös paljon muutakin, kuten vaikkapa muuttaa sivuston indeksi-tiedoston ohjaamaan
sivuston vierailiat minne vaan.
**upload.php** on kaikinpuolin hyvin haavoittuvainen kaikenlaisille hyökkäyksille.
 
 ---
 Tiedosto **secureupload.php** ja kansio **secure_uploads** sisältävät monia tapoja 
 turvata tiedostonlatausta palvelimelle. Nämä ovat:
 
 * **php.ini** asetusten määrittely
    - Ladattavien tiedostojen maksimikoko kannattaa rajoittaa
    - Asettamalla **expose_php** pois päältä palvelin ei paljasta php:n olevan palvelimella käytössä
 * tiedoston koon tarkistus
 * tiedostonnimen muuttaminen
    - Tiedosto nimen muuttaminen satunnaiseksi merkkijonoksi estää erilaiset XSS-hyökkäykset (esim. tiedoston nimessä) ja esim. .httaccess tiedoston lataamisen palvelimelle
 * tiedostotyypin tarkistus
    - Tiedoston tiedostotyyppi kannattaa tarkistaa tarkistamalla tiedoston MIME-tyyppi (Multipurpose Internet Mail Extensions), tiedoston päätteestä ei kuitenkaan voi varmistaa tiedoston todellista tyyppiä
 * tiedoston oikeuksien muuttaminen
    -  tiedostolta evätään suoritysoikeudet, muuttamalla oikeuksia *chmod()*-funktiolla
 * tiedoston siirto **www** -kansion ulkopuolelle (Meidän projektissa ei mahdollinen)
    - tiedostot ovat hyvä varmuuden vuoksi siirtää kansioon johon ei ole suoraa pääsyä ja jossa ei tiedostoja ei missään tapauksessa voi avata
 * **.htaccess** asetusten muuttaminen
    - http headereitten asettaminen estääkseen mm. XSS-hyökkäyksiä ja pakottamaan tiedostojen lataus avaamisen sijasta
    - tiedoston suorituksen esto

 
##### secureupload.php, upload.php ja .htaccess sisältävät kommentteja eri turvatoimenpiteistä ja miten ne käytännössä toteutetaan

##### Viitteitä

https://stackoverflow.com/questions/256172/what-is-the-most-secure-method-for-uploading-a-file
https://scotthelme.co.uk/hardening-your-http-response-headers/