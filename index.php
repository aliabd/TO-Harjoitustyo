<!DOCTYPE html>
<html>
<body>
    
<h1>Insecure Form</h1>
<form action="upload.php" method="post" enctype="multipart/form-data">
    Select image file to upload:
    <input type="file" name="fileToUpload" id="fileToUpload">
    <input type="submit" value="Upload File" name="submit">
</form>

</br></br>

<h1>Secure Form</h1>
<form action="secureupload.php" method="post" enctype="multipart/form-data">
    Select image file to upload:
    <input type="file" name="fileToUpload" id="fileToUpload">
    <input type="submit" value="Upload File" name="submit">
</form>

</body>
</html>;